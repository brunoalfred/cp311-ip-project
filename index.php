<?php
// require once the database.php file
require_once 'database.php';
require 'helpers.php';


// check if the form is submitted with POST method

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // if the form is submitted, then add the data into the database
    addEntry($conn);
} else if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    if (isset($_REQUEST['last-name'])) {
        // if yes, then get the value of the parameter
        $last_name = $_GET['last-name'];

        $lastNameHint = '';

        // check if the value of the parameter is not empty
        if (!empty($last_name)) {
            // if not empty, then search for the last name in the database
            $sql = "SELECT last_name FROM phone_book WHERE last_name LIKE '%$last_name%'";
            $result = $conn->query($sql);


            // return the results to the browser
            if ($result->num_rows > 0) {

                $last_name = strtolower($last_name);
                $length = strlen($last_name);

                foreach ($result as $row) {

                    if (stristr($last_name, substr($row['last_name'], 0, $length))) {
                        if ($lastNameHint === "") {
                            $lastNameHint = $row['last_name'];
                        } else {
                            $lastNameHint .= ", $row[last_name]";
                        }
                    }
                }

                echo $lastNameHint === "" ? "No likely results" : $lastNameHint;
            } else {
                echo "0 results";
            }
        }
    } else {
        getEntries($conn);
    }

    // if the form is submitted with GET method, then display the data from the database

} else {
    echo "invalid request method";
    exit();
}






// close the connection
$conn->close();
