<?php

//  create database connection on database  phone_book_db running localhost with root user   and  pleaseletmein as password 

$conn = new mysqli("localhost", "root", "pleaseletmein", "phone_book_db");

// check connection is successful or not
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    echo "Connected successfully";
}

// check if table  "phone_book" exists or not
// create database table named "phone_book" with table fields
// first_name(VARCHAR) 30 characters, last_name(VARCHAR) 30 characters,
//  phone_number(VARCHAR) 10 characters

$sql = "CREATE TABLE IF NOT EXISTS phone_book (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(30) NOT NULL,
    last_name VARCHAR(30) NOT NULL,
    phone_number VARCHAR(10) NOT NULL
    )";

// check if table is created successfully or not
if ($conn->query($sql) === TRUE) {
    echo "Table phone_book created successfully" . "<br>";
} else {
    echo "Error creating table: " . $conn->error . "<br>";
}
