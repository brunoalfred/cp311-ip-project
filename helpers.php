<?php



function getEntries($conn)
{
    $sql = "SELECT * FROM phone_book";
    $result = $conn->query($sql);

    // return the results to the browser
    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            echo "[First Name: " . $row["first_name"] . " - Last Name: " . $row["last_name"] . " - Phone Number: " . $row["phone_number"] . "]" . "<br>";

        }
    } else {
        echo "0 results";
    }
}

function addEntry($conn)
{
    // check if the form is submitted with empty fields
    if (empty($_POST['first-name']) || empty($_POST['last-name']) || empty($_POST['phone-number'])) {
        exit();
    } else {

        // if not empty, then insert the data into the database
        $first_name = $_POST['first-name'];
        $last_name = $_POST['last-name'];
        $phone_number = $_POST['phone-number'];


        // check if phone number doesnt exceed 10 digits
        if (strlen($phone_number) > 10) {
            echo "Phone number should not exceed 10 digits";
        } else {
            // if not, then insert the data into the database
            $sql = "INSERT INTO phone_book (first_name, last_name, phone_number) VALUES ('$first_name', '$last_name', '$phone_number')";

            // check if data is inserted successfully or not
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }
    }
}
